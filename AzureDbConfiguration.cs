﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;

namespace StudioKit.Data.Entity
{
	/// <summary>
	/// A <see cref="DbConfiguration"/> for use in the Azure environment.
	/// Handle transient errors and works with <see cref="SuspendableSqlAzureExecutionStrategy"/>
	/// to allow suspension in order to use a <see cref="DbContextTransaction"/>.
	/// </summary>
	public class AzureDbConfiguration : DbConfiguration
	{
		public AzureDbConfiguration()
		{
			SetExecutionStrategy(SqlProviderServices.ProviderInvariantName, () => IsExecutionStrategySuspended
				? (IDbExecutionStrategy)new DefaultExecutionStrategy()
				: new SqlAzureExecutionStrategy());
		}

		public static bool IsExecutionStrategySuspended
			=> (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? false;
	}
}