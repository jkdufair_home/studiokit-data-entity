﻿using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Utilities;
using System.Data.Entity.SqlServer;

namespace StudioKit.Data.Entity
{
	public class DefaultDateSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
	{
		protected override void Generate(ColumnModel column, IndentedTextWriter writer)
		{
			SetDateColumnDefault(column);
			base.Generate(column, writer);
		}

		private static void SetDateColumnDefault(PropertyModel column)
		{
			if (column.Name == "DateStored" || column.Name == "DateLastUpdated")
			{
				column.DefaultValueSql = "GETUTCDATE()";
			}
		}
	}
}