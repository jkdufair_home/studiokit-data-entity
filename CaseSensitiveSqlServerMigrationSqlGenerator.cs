﻿using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;

namespace StudioKit.Data.Entity
{
	public class CaseSensitiveSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
	{
		protected override void Generate(AlterColumnOperation alterColumnOperation)
		{
			base.Generate(alterColumnOperation);
			AnnotationValues values;
			if (!alterColumnOperation.Column.Annotations.TryGetValue("CaseSensitive", out values)) return;
			if (values.NewValue == null || values.NewValue.ToString() != "True") return;
			using (var writer = Writer())
			{
				var columnSql = BuildColumnType(alterColumnOperation.Column);
				writer.WriteLine(
					"ALTER TABLE {0} ALTER COLUMN {1} {2} COLLATE SQL_Latin1_General_CP1_CS_AS {3}",
					alterColumnOperation.Table,
					alterColumnOperation.Column.Name,
					columnSql,
					alterColumnOperation.Column.IsNullable.HasValue == false || alterColumnOperation.Column.IsNullable.Value == true
						? " NULL"
						: "NOT NULL" // TODO: not tested for DefaultValue
					);
				Statement(writer);
			}
		}

		protected override void Generate(CreateTableOperation createTableOperation)
		{
			base.Generate(createTableOperation);
			foreach (var column in createTableOperation.Columns)
			{
				AnnotationValues values;
				if (!column.Annotations.TryGetValue("CaseSensitive", out values)) continue;
				if (values.NewValue == null || values.NewValue.ToString() != "True") continue;
				using (var writer = Writer())
				{
					var columnSql = BuildColumnType(column);
					writer.WriteLine(
						"ALTER TABLE {0} ALTER COLUMN {1} {2} COLLATE SQL_Latin1_General_CP1_CS_AS {3}",
						createTableOperation.Name,
						column.Name,
						columnSql,
						column.IsNullable.HasValue == false || column.IsNullable.Value == true ? " NULL" : "NOT NULL"
						// TODO: not tested for DefaultValue
						);
					Statement(writer);
				}
			}
		}
	}
}