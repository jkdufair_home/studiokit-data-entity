﻿# StudioKit.Data.Entity

Contains basic EntityFramework models and configuration options.

* **AzureDbConfiguration**
	* Use as the base class for your DbConfiguration. Sets an Execution Strategy suitable for cloud environments, such as Azure.