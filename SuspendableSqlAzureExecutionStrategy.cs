﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Data.Entity
{
	/// <summary>
	/// <see cref="IDbExecutionStrategy"/> which allows suspension
	/// in order to use a <see cref="DbContextTransaction"/>.
	/// Decorates <see cref="SqlAzureExecutionStrategy"/>.
	/// </summary>
	public class SuspendableSqlAzureExecutionStrategy : IDbExecutionStrategy
	{
		private readonly IDbExecutionStrategy _azureExecutionStrategy;

		public SuspendableSqlAzureExecutionStrategy()
		{
			_azureExecutionStrategy = new SqlAzureExecutionStrategy();
		}

		private static bool Suspend
		{
			get { return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? false; }
			set { CallContext.LogicalSetData("SuspendExecutionStrategy", value); }
		}

		public bool RetriesOnFailure => !Suspend;

		public virtual void Execute(Action operation)
		{
			if (!RetriesOnFailure)
			{
				operation();
				return;
			}

			try
			{
				Suspend = true;
				_azureExecutionStrategy.Execute(operation);
			}
			finally
			{
				Suspend = false;
			}
		}

		public virtual TResult Execute<TResult>(Func<TResult> operation)
		{
			if (!RetriesOnFailure)
			{
				return operation();
			}

			try
			{
				Suspend = true;
				return _azureExecutionStrategy.Execute(operation);
			}
			finally
			{
				Suspend = false;
			}
		}

		public virtual async Task ExecuteAsync(Func<Task> operation, CancellationToken cancellationToken)
		{
			if (!RetriesOnFailure)
			{
				await operation().ConfigureAwait(false);
				return;
			}

			try
			{
				Suspend = true;
				await _azureExecutionStrategy.ExecuteAsync(operation, cancellationToken).ConfigureAwait(false);
			}
			finally
			{
				Suspend = false;
			}
		}

		public virtual async Task<TResult> ExecuteAsync<TResult>(Func<Task<TResult>> operation,
			CancellationToken cancellationToken)
		{
			if (!RetriesOnFailure)
			{
				return await operation().ConfigureAwait(false);
			}

			try
			{
				Suspend = true;
				return await _azureExecutionStrategy.ExecuteAsync(operation, cancellationToken).ConfigureAwait(false);
			}
			finally
			{
				Suspend = false;
			}
		}
	}
}