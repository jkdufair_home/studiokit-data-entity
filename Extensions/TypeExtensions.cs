﻿using System;

namespace StudioKit.Data.Entity.Extensions
{
	public static class TypeExtensions
	{
		private const string DynamicProxyNamespace = "System.Data.Entity.DynamicProxies";

		/// <summary>
		/// Compare two Entity <see cref="Type"/>s to see if they are equal, and handle if either is an EntityFramework Dynamic Proxy from System.Data.Entity.DynamicProxies.
		/// </summary>
		/// <param name="x">The first type</param>
		/// <param name="y">The second type</param>
		/// <returns></returns>
		public static bool EntityTypeEquals(this Type x, Type y)
		{
			var xType = x.Namespace == DynamicProxyNamespace && x.BaseType != null
				? x.BaseType
				: x;
			var yType = y.Namespace == DynamicProxyNamespace && y.BaseType != null
				? y.BaseType
				: y;
			return xType == yType;
		}
	}
}